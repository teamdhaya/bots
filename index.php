<?php
include_once("common.php");
$verify_token = ""; // Verify token
$token = ""; // Page token

if (file_exists(__DIR__.'/config.php')) {
    $config = include __DIR__.'/config.php';
    $verify_token = $config['verify_token'];
    $token = $config['token'];
}

require_once(dirname(__FILE__) . '/autoload.php');

use pimax\FbBotApp;
use pimax\Messages\Message;
use pimax\Messages\MessageButton;
use pimax\Messages\StructuredMessage;
use pimax\Messages\MessageElement;
use pimax\Messages\MessageReceiptElement;
use pimax\Messages\Address;
use pimax\Messages\Summary;
use pimax\Messages\Adjustment;

$bot = new FbBotApp($token);


if (!empty($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe' && $_REQUEST['hub_verify_token'] == $verify_token)
{
	// Webhook setup request
	echo $_REQUEST['hub_challenge'];
} else {
	$data = json_decode(file_get_contents("php://input"), true);
	write_log(serialize($data), 'SUCCESS');
	if (!empty($data['entry'][0]['messaging'])) {
		foreach ($data['entry'][0]['messaging'] as $message) {
			if(!empty($message) && !empty($message['message']['text']) && empty($message['message']['is_echo'])) {
				write_log(serialize($message), 'SUCCESS');

				$bot->send(new Message($message['sender']['id'], 'Hi there!'));
			}
		}
	} else {
		echo 'Till this place';
	}
}

?>